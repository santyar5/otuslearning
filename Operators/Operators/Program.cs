﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operators
{
    public class Point3D
    {
        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }

        /// <summary>
        /// точка на координатной плоскости xyz
        /// </summary>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        public Point3D (double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        private Point3D[] arr;
        public Point3D()
        {
            arr = new Point3D[100];
        }

        public Point3D this[int i]
        {
            get { return arr[i]; }
            set { arr[i] = value; }
        }
        /// <summary>
        /// сумма 2х точек
        /// </summary>
        /// <param name="pointA">точка 1</param>
        /// <param name="pointB">точка 2</param>
        /// <returns>new Point3D</returns>
        public static Point3D operator +(Point3D pointA, Point3D pointB)
        {
            return new Point3D(pointA.x + pointB.x, pointA.y + pointB.y, pointA.z + pointB.z);
        }
        /// <summary>
        /// разница 2х точек
        /// </summary>
        /// <param name="pointA">точка 1</param>
        /// <param name="pointB">точка 2</param>
        /// <returns>new Point3D</returns>
        public static Point3D operator -(Point3D pointA, Point3D pointB)
        {
            return new Point3D(pointA.x - pointB.x, pointA.y - pointB.y, pointA.z - pointB.z);
        }
        /// <summary>
        /// инверсия точки
        /// </summary>
        /// <param name="point">точка</param>
        /// <returns>new Point3D</returns>
        public static Point3D operator -(Point3D point)
        {
            return new Point3D(-1 * point.x, -1 * point.y, -1 * point.z);
        }
        /// <summary>
        /// умножение точки на целый коэффициент
        /// </summary>
        /// <param name="point">точка</param>
        /// <param name="koef">коэффициент</param>
        /// <returns>new Point3D</returns>
        public static Point3D operator *(Point3D point, double koef)
        {
            return new Point3D(point.x * koef, point.y * koef, point.z * koef);
        }
        /// <summary>
        /// преобразование точки в строковое значение
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return $"[{x};{y};{z}]";
        }
    }
    public static class Point3DExtension
    {
        public static Point3D Sqrt(this Point3D point)
        {
            double a = Math.Round(Math.Sqrt(point.x) * 100) / 100;
            double b = Math.Round(Math.Sqrt(point.y) * 100) / 100;
            double c = Math.Round(Math.Sqrt(point.z) * 100) / 100;
            return new Point3D(a, b, c);
        }
    }
    static class Program
    {
        public static void Main(string[] args)
        {
            Point3D points = new Point3D();
            points[0] = new Point3D(1, 1, 1);
            points[1] = new Point3D(5, 7, 11);
            double koef = 3;

            Console.WriteLine("Points Summ: " + (points[0] + points[1]).ToString());
            Console.WriteLine("Points Diff1: " + (points[0] - points[1]).ToString());
            Console.WriteLine("Points Diff2: " + (points[1] - points[0]).ToString());
            Console.WriteLine("Point invert: " + (-points[0]).ToString() + " " + (-points[1]).ToString());
            Console.WriteLine("Point Mult1: " + (points[0] * koef).ToString());
            Console.WriteLine("Point Mult2: " + (points[1] * koef).ToString());
            Console.WriteLine(points[1].Sqrt().ToString());
            Console.ReadLine();
        }
    }
}
